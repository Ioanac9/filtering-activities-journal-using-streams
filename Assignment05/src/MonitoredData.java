import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.time.LocalDateTime;

public class MonitoredData {
	private LocalDateTime startTime;
	private LocalDateTime endTime;
	private String activity;
	private int days;
	List<MonitoredData> monitoredData = new ArrayList<MonitoredData>();
	
	public MonitoredData (LocalDateTime startTime,LocalDateTime endTime,String activity ){
		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;
	}
	
	public LocalDateTime getStartTime() {
		return startTime;
	}
	public LocalDateTime getEndTime() {
		return endTime;
	}
	public String getActivity() {
		return activity;
	}
	
	public int getDays() {
		return days;
	}
	 public  Map<String, Integer> numarActivitatiTotal(){
		    
		   Map<String, Integer> map=new HashMap<>();
		   map= Main.list
				   .stream()
				   .collect(Collectors.groupingBy(p->p.getActivity(),Collectors.summingInt(p->1))); //p=p+1 ,echivalent cu count
		   
		   System.out.println("\n\nNumarul de actiuni desfasurate pentru fiecare activitate : ");
		   map.forEach((k, v) -> System.out.println((k + ":" + v))); //afisare folosind lambda
		return map;
	   }	
		
	
}