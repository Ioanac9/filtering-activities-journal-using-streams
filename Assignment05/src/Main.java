import java.util.List;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.ZoneOffset;
import java.util.Map;
import java.util.Map.Entry;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.JOptionPane;

public class Main {
	static List<MonitoredData> list = new ArrayList<>();
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyy-MM-dd HH:mm:ss");
	
	//task1
	void citireFile(String fileName) {
		List<String> lista = new ArrayList<>();

		try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName))) {

			lista = br.lines().collect(Collectors.toList());

		} catch (IOException e) {
			e.printStackTrace();
		}

		
		lista.forEach(subline-> {
			    String[] start = subline.substring(0).split("\t"); //merge pana la primul tab si separa in timeStart
				String[] end = subline.substring(21).split("\t"); //merge de la caracterul21 din lista curenta(prima pozitie dupa
				                                            //primul tab) si pana la al doilea tab intalnit 
				String[] activitate = subline.substring(42).split("\t");//merge de la inceputul primului caracter dorit pana la sfarsit
				
				LocalDateTime dateEnd = LocalDateTime.parse(end[0], formatter);//transforma subsirul in data //de linie
				LocalDateTime dateStart = LocalDateTime.parse(start[0], formatter); //transforma subsirul in data
				
				MonitoredData md = new MonitoredData(dateStart,dateEnd,activitate[0]);//se creeaza un obiect 
				list.add(md);});	//se adauga in lista de obiecte de tipul MonitorizedData
				
			}
	//task2
	public void countDays(){
		long nrZileDistincte;
		List<LocalDateTime> listaDataStart = new ArrayList<>();
		listaDataStart= list.stream().map(list->list.getStartTime()).collect(Collectors.toList());
		
		nrZileDistincte=listaDataStart.stream()
				.map(line -> line.getDayOfYear()) 	              
				.distinct()          
				.count();
		
	System.out.println("\n\nNumarul de zile monitorizate care apar in jurnal sunt: "+nrZileDistincte);	
}
    //task3
    public void numarActivitatiTotal(){
    
	   Map<String, Integer> map=new HashMap<>();
	   map= list
			   .stream()
			   .collect(Collectors.groupingBy(p->p.getActivity(),Collectors.summingInt(p->1))); //p=p+1 ,echivalent cu count
	   
	   System.out.println("\n\nNumarul de actiuni desfasurate pentru fiecare activitate : ");
	   map.forEach((k, v) -> System.out.println((k + ":" + v))); //afisare folosind lambda
   }	   
	//task4
   public void numarActivitatiPeZi() {
	   
	   @SuppressWarnings("unused")
	Map<String, Integer> map1=new HashMap<>();
	  
	   Map<Object, Map<Object, Integer>> map =list
			   .stream()
			   .collect(Collectors.groupingBy(line->line.getStartTime().getDayOfYear(),
					   Collectors.groupingBy(line->line.getActivity(),Collectors.summingInt(line->1))));
	   
	   System.out.println("\n\nNumarul de actiuni desfasurate pentru fiecare activitate pe zi : ");
	   for(Entry<Object, Map<Object, Integer>> m :map.entrySet()){	 
			System.out.println("\n"+Year.of( 2011 ).atDay( (int) m.getKey() )+"="+m.getValue());
	   }
	   	   
   }
   //task5
   public void durataActivitatiOverTenHours() {
	   Map<Object, Double> mapas =list
	           .stream()
	           .collect(Collectors.groupingBy(line->line.getActivity(),//se ia fiecare linie si se grupeaza cu suma 
						Collectors.summingDouble(line->java.time.Duration.between(line. //duratei intre timp sf si cel de inc
						getStartTime(),line.getEndTime()).toHours())));//si apoi se filtreaza din stream doar valorile>=10

	   Map<Object, Double> collect=mapas
			  .entrySet()
			  .stream()
			  .filter(x -> x.getValue() > 10)
			  .collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue()));
	   
	  System.out.println("\n\nActiunile cu timp de desfasurare mai mare de 10 ore: ");
	   collect.forEach((k, v) -> System.out.println((k + ":" + v))); //afisare folosind lambda
   }
   //task6
   public void durataActivitatiLessFiveMin() {
	   
	   Map<String, Integer> map1=new HashMap<>();
	   map1= list
			   .stream()
			   .collect(Collectors.groupingBy(p->p.getActivity(),Collectors.summingInt(p->1)));
	   
	   Map<String, Double> mapas =list
	           .stream()
	           .collect(Collectors.groupingBy(line->line.getActivity(),//se ia fiecare linie si se grupeaza cu suma 
						Collectors.summingDouble(line->java.time.Duration.between(line. //duratei intre timp sf si cel de inc
						getStartTime(),line.getEndTime()).toMinutes())));//si apoi se filtreaza din stream doar valorile>=10
		 
		List<String> listas = new ArrayList<String>();
		for(String s: mapas.keySet()){
				if(mapas.get(s) <=5)
					for(Map.Entry entry:map1.entrySet())
						if(s.equals(entry.getKey()))
					      if(map1.get(s)*0.9 >= mapas.get(entry.getKey()))
				            listas.add(s);
		}  
	   
	  System.out.println("\n\nActiunile cu timp de desfasurare mai mic de 5 minute: ");
	  System.out.println(listas);
   }
   
   
   public static void main(String[] args)  {
		Main main = new Main();
		String fileName = "Activities.txt";
		
		main.citireFile(fileName);
		main.countDays();	
		main.numarActivitatiTotal();
		main.numarActivitatiPeZi();
		main.durataActivitatiOverTenHours();
		main.durataActivitatiLessFiveMin();
  
   }
}